# Fedora had it in F17, but moving things around in already-released versions
# is a bad idea, so pretend it didn't happen until F19.
%if 0%{?fedora} > 18 || 0%{?rhel} > 6
%global separate_usr 0
%global nssdir %{_libdir}
%global pamdir %{_libdir}/security
%else
%global separate_usr 1
%global nssdir /%{_lib}
%global pamdir /%{_lib}/security
%endif

# For distributions that support it, build with RELRO
%if (0%{?fedora} > 15 || 0%{?rhel} >= 7)
%define _hardened_build 1
%endif

Name:		aehostd-modules
Version:	0.9.9
Release:	0%{?dist}
Summary:	NSS and PAM modules for aehostd (AE-DIR)
Group:		System Environment/Base
License:	LGPLv2+
URL:		https://arthurdejong.org/nss-pam-ldapd/
Source0:	https://arthurdejong.org/nss-pam-ldapd/nss-pam-ldapd-%{version}.tar.gz
Source1:	https://arthurdejong.org/nss-pam-ldapd/nss-pam-ldapd-%{version}.tar.gz.sig

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	openldap-devel
BuildRequires:	autoconf, automake
BuildRequires:	pam-devel
Requires(post):		/sbin/ldconfig
Requires(postun):	/sbin/ldconfig

%description
The front-end modules used to use NSS and PAM via aehostd.

This package is only useful when using AE-DIR as LDAP server.

%prep
%setup -qn nss-pam-ldapd-%{version}
autoreconf -f -i

%build
CFLAGS="$RPM_OPT_FLAGS -fPIC" ; export CFLAGS
%configure \
  --with-module-name=aedir \
  --disable-nslcd --disable-pynslcd --disable-kerberos \
  --libdir=%{nssdir} \
  --with-pam-seclib-dir=%{pamdir} \
  --disable-utils \
  --with-nss-maps=passwd,group,hosts \
  --with-ldap-lib=openldap \
  --with-nslcd-socket=/var/run/aehostd/aehostd.sock
make %{?_smp_mflags}

%check

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_libdir}
rm $RPM_BUILD_ROOT/etc/nslcd.conf

%if 0%{?fedora} > 13 || 0%{?rhel} > 5
%if %{separate_usr}
# Follow glibc's convention and provide a .so symlink so that people who know
# what to expect can link directly with the module.
if test %{_libdir} != /%{_lib} ; then
	touch $RPM_BUILD_ROOT/rootfile
	relroot=..
	while ! test -r $RPM_BUILD_ROOT/%{_libdir}/$relroot/rootfile ; do
		relroot=../$relroot
	done
	ln -s $relroot/%{_lib}/libnss_aedir.so.2 \
		$RPM_BUILD_ROOT/%{_libdir}/libnss_aedir.so
	rm $RPM_BUILD_ROOT/rootfile
fi
%else
ln -s libnss_aedir.so.2 $RPM_BUILD_ROOT/%{nssdir}/libnss_aedir.so
%endif
%endif

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc AUTHORS ChangeLog COPYING HACKING NEWS README TODO
%{nssdir}/*.so*
%{pamdir}/pam_aedir.so
%{_mandir}/*/*

%pre

%post
/sbin/ldconfig

%preun

%postun
/sbin/ldconfig

%changelog
* Wed Jul 18 2018 Michael Stroeder <michael@stroeder.com> - 0.9.9-0
- initial package
