############################################################################
# Infrastructure LDAP server example (schema for PowerDNS and ISC dhcpd)
#
# - uses AE-DIR consumer for authentication and authorization
#   for administrative access
# - local TLS server cert/key is used as client cert/key for
#   binding as aeService with SASL/EXTERNAL
############################################################################

loglevel stats

include /etc/openldap/schema/core.schema
include /etc/openldap/schema/cosine.schema
include /etc/openldap/schema/inetorgperson.schema
include /etc/openldap/schema/nis.schema
include /etc/openldap/schema/misc.schema
include /etc/openldap/schema/dhcp.schema
include /etc/openldap/local-schema/dnsdomain2.schema
include /etc/openldap/local-schema/draft-findlay-ldap-groupofentries.schema

threads 4
pidfile /run/slapd/slapd.pid
argsfile /run/slapd/slapd.args

#---------------------------------------------------------------------------
# Load dynamic backend modules:
#---------------------------------------------------------------------------

moduleload back_mdb
moduleload back_ldap
moduleload back_monitor

#---------------------------------------------------------------------------
# Load dynamic overlay modules:
#---------------------------------------------------------------------------

moduleload syncprov
moduleload unique
moduleload rwm

#---------------------------------------------------------------------------
# password hash parameters
#---------------------------------------------------------------------------

password-hash {CRYPT}
# SHA-512, 72 bits) of salt, 5000 iterations
password-crypt-salt-format "$6$%.12s"

#---------------------------------------------------------------------------
# TLS parameters
#---------------------------------------------------------------------------

# require at least TLS 1.2
TLSProtocolMin 3.3
TLSVerifyClient allow
#TLSCipherSuite DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:AES256-SHA:!ADH

TLSCACertificateFile /etc/openldap/tls/my-super-power-ca.pem
TLSCertificateFile /etc/openldap/tls/infra-ldap.example.net.crt
TLSCertificateKeyFile /etc/openldap/tls/infra-ldap.example.net.key
TLSDHParamFile /etc/openldap/tls/slapd.dhparam

#---------------------------------------------------------------------------
# Rewrite SASL identity to DIT identity
#---------------------------------------------------------------------------

# Map root user to rootdn when SASL/EXTERNAL is used with LDAPI
authz-regexp
  "gidnumber=0\\+uidnumber=0,cn=peercred,cn=external,cn=auth"
  "cn=root,dc=example,dc=net"

# Map user/group to existing posixAccount entry when SASL/EXTERNAL is used with LDAPI
authz-regexp
  "gidnumber=([0-9]+)\\+uidnumber=([0-9]+),cn=peercred,cn=external,cn=auth"
  "ldap:///dc=example,dc=net??sub?(&(objectClass=posixAccount)(uidNumber=$2))"

#---------------------------------------------------------------------------
# Global access control and security restrictions
#---------------------------------------------------------------------------

# Strict bind requirements
disallow bind_anon
require bind LDAPv3 strong

# Access to rootDSE for all authenticated users
access to
  dn.base=""
    by users read

# Access to subschema subentry for all authenticated users
access
  to dn.base="cn=Subschema"
    by users read

############################################################################
# Configuration backend cn=config
############################################################################

database config

access to
  dn.subtree="cn=config"
    by group/groupOfEntries/member="cn=infra-admins,cn=infra,ou=ae-dir" read
    by dn.exact="cn=root,dc=example,dc=net" read
    by * none

############################################################################
# The main backend dc=example,dc=net
############################################################################

database  mdb
suffix    "dc=example,dc=net"
directory /var/lib/ldap

# database tuning
maxsize 4000000
dbnosync
checkpoint 20000 1

lastmod on
monitoring on

rootdn    "cn=root,dc=example,dc=net"
# Do not enable!
#rootpw   secret

# Indexing
index objectClass eq
index uid eq
index uidNumber eq
# for syncrepl
index entryUUID,entryCSN eq
# For PowerDNS authorative server
index sOARecord,nSRecord,aRecord,aAAARecord,pTRRecord,associatedDomain pres,eq
# For DHCP server
index dhcpHWAddress,dhcpClassData eq

access to
  dn.subtree="dc=example,dc=net"
    by group="cn=slapd-replicas,ou=groups,dc=example,dc=net" read
    by * break

access to
  attrs=userPassword
    by group/groupOfEntries/member="cn=infra-admins,cn=infra,ou=ae-dir" =wx
    by self =wx
    by anonymous auth

access to
  dn.subtree="dc=example,dc=net"
  attrs=entry
    by group/groupOfEntries/member="cn=infra-admins,cn=infra,ou=ae-dir" manage
    by users read

access to
  dn.subtree="dc=example,dc=net"
    by group/groupOfEntries/member="cn=infra-admins,cn=infra,ou=ae-dir" manage
    by users read
    by * none

overlay unique
unique_uri "ldap:///dc=example,dc=net?uid,uidNumber,dhcpHWAddress?sub?(objectClass=*)"

overlay syncprov
syncprov-checkpoint 20 2
syncprov-sessionlog 400

############################################################################
# Proxy backend for ou=ae-dir
############################################################################

database ldap

suffix ou=ae-dir

uri ldaps://ae-dir-c1.m1.hv.local
protocol-version 3
proxy-whoami yes
session-tracking-request yes
norefs yes
idle-timeout 600
omit-unknown-schema yes

rootdn "uid=system_infra1_slapd,ou=ae-dir"

acl-bind
  bindmethod=sasl
  saslmech=EXTERNAL
  tls_cert=/etc/openldap/tls/infra-ldap.example.net.crt
  tls_key=/etc/openldap/tls/infra-ldap.example.net.key
  tls_cacert=/etc/openldap/tls/my-super-power-ca.pem
  tls_reqcert=demand

overlay rwm
rwm-rewriteEngine on

# Bind-DN rewriting with internal searches (before external bind listener)
# uid=foo,ou=ae-dir -> entryDN of entry within ou=ae-dir matching (uid=foo)
rwm-rewriteMap slapd uid2dn "ldap:///ou=ae-dir?entryDN?sub?"
rwm-rewriteContext bindDN
rwm-rewriteRule "^(uid=[^,]+),ou=ae-dir$" "${uid2dn($1)}" ":@I"

chase-referrals no

access to
  dn.subtree="ou=ae-dir"
    by group/groupOfEntries/member="cn=infra-admins,cn=infra,ou=ae-dir" read
    by * auth

############################################################################
# Monitoring backend cn=monitor
############################################################################

database monitor

access to
  dn.subtree="cn=monitor"
    by dn.exact="cn=root,dc=example,dc=net" read
    by group/groupOfEntries/member="cn=infra-admins,cn=infra,ou=ae-dir" read
