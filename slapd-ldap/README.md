An LDAP server using back-ldap proxying Æ-DIR for authc/authz administrative access

Please study file _slapd.conf_ carefully.

See picture: https://www.ae-dir.com/apps.html#slapd-ldap

You have to add a reasonable tree structure within the backend
_dc=example,dc=net_ which also should contain a "local" LDAP service
account for clients. This avoids transitive failures in case AE-DIR is
unreachable.

The local LDAP slapd authenticates itself with own server cert used
as TLS client cert. No service password is needed.

For this to work the subject-DN *must* be stored in attribute _seeAlso_ in
the _aeService_ entry as LDAPv3 DN string representation.

Furthermore the password policy is set to pwdPolicySubentry which
effectively disables password authc.

```
dn: uid=system_infra1_slapd,cn=infra-ldap,cn=infra,ou=ae-dir
seeAlso: cn=infra-ldap.infra.example.com,ou=infra,o=Company,c=DE
pwdPolicySubentry: cn=ppolicy-sasl-external,cn=ae,ou=ae-dir
[..]
```

You can determine the cert subject-DN with this command:

```
openssl x509 -noout -name rfc2253 -subject -in /etc/openldap/tls/infra-ldap.example.net.crt
```
