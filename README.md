Æ-DIR -- Client Configuration and Installation
==============================================

This directory contains various configuration examples for different types
of components.

The file are only meant as inspiration.  Of course you have
to tweak these to fit your environment to be usable.

### client-examples/apache/

  Example configuration files for Apache httpd.

### client-examples/sssd/

  Example configuration files for sssd:

   - with simple authentication (bind-DN and password)
   - with TLS client certs and SASL/EXTERNAL (needs private key and X.509 cert)

### client-examples/debian/

  Example deployment files for Debian Jessie run with libvirt

### client-examples/opensuse/

  Example deployment files for openSUSE Tumbleweed run with libvirt

