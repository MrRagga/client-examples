QEMU_URL="qemu:///system"

#DOWNLOAD_HOST="download.opensuse.org"
DOWNLOAD_HOST="10.54.1.1"

# tumbleweed
REPO_URL="http://${DOWNLOAD_HOST}/tumbleweed/repo/oss"
# Leap 42.1
#REPO_URL="http://${DOWNLOAD_HOST}/distribution/leap/42.1/repo/oss"

VTYPE="kvm"
VNET="virtnet1"
MAC_ADDRESS="52:54:00:23:42:01"

virt-install \
  --connect="${QEMU_URL}" \
  --virt-type "${VTYPE}" \
  --name ae-client-opensuse \
  --cpu=host \
  --ram 1024 \
  --disk path=/var/lib/libvirt/images/ae-client-opensuse.qcow2,format=qcow2,bus=virtio,size=6\
  --boot hd \
  --network network=${VNET},mac=${MAC_ADDRESS} \
  --location="${REPO_URL}" \
  --extra-args "autoyast=file:////autoyast-ae-client.xml textmode=1" \
  --initrd-inject=autoyast-ae-client.xml
